package ru.t1.dkozoriz.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.dkozoriz.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.dkozoriz.tm.api.repository.model.ISessionRepository;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.model.ISessionService;
import ru.t1.dkozoriz.tm.model.Session;
import ru.t1.dkozoriz.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;

@Service
@NoArgsConstructor
public final class SessionService extends UserOwnedService<Session> implements ISessionService {

    private final static String NAME = "Session";

    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    protected ISessionRepository getRepository() {
        return context.getBean(ISessionRepository.class);
    }

}