package ru.t1.dkozoriz.tm.api.repository.dto.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDto;

import java.util.List;

public interface ITaskDtoRepository extends IBusinessDtoRepository<TaskDto> {
    @NotNull List<TaskDto> findAllByProjectId(@Nullable String userId, @NotNull String projectId);
}
