package ru.t1.dkozoriz.tm.service.dto.business;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.dkozoriz.tm.api.repository.dto.business.IProjectDtoRepository;
import ru.t1.dkozoriz.tm.api.service.dto.business.IProjectDtoService;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDto;
import ru.t1.dkozoriz.tm.exception.field.DescriptionEmptyException;
import ru.t1.dkozoriz.tm.exception.field.NameEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;

import javax.persistence.EntityManager;

@Service
@NoArgsConstructor
public final class ProjectDtoService extends BusinessDtoService<ProjectDto> implements IProjectDtoService {

    private final static String NAME = "Project";

    @NotNull
    @Override
    protected IProjectDtoRepository getRepository() {
        return context.getBean(IProjectDtoRepository.class);
    }

    @NotNull
    public String getName() {
        return NAME;
    }

    @NotNull
    public ProjectDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDto project = new ProjectDto();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        add(userId, project);
        return project;
    }

}