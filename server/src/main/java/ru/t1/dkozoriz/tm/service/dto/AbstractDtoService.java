package ru.t1.dkozoriz.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.dkozoriz.tm.api.repository.dto.IAbstractDtoRepository;
import ru.t1.dkozoriz.tm.api.service.dto.IAbstractDtoService;
import ru.t1.dkozoriz.tm.dto.model.AbstractModelDto;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.IndexIncorrectException;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDtoService<T extends AbstractModelDto> implements IAbstractDtoService<T> {

    @Getter
    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    protected abstract String getName();

    @NotNull
    protected abstract IAbstractDtoRepository<T> getRepository();

    @Override
    @Nullable
    public T add(@Nullable final T model) {
        if (model == null) return null;
        @NotNull final IAbstractDtoRepository<T> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public void update(@NotNull final T model) {
        @NotNull final IAbstractDtoRepository<T> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final IAbstractDtoRepository<T> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public List<T> findAll() {
        @NotNull final IAbstractDtoRepository<T> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final T model) {
        @NotNull final IAbstractDtoRepository<T> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final T model = findById(id);
        if (model == null) return;
        remove(model);
    }

    @Override
    public void removeByIndex(@Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize()) throw new IndexIncorrectException();
        @Nullable final T model = findByIndex(index);
        if (model == null) return;
        remove(model);
    }

    @Override
    @Nullable
    public T findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final IAbstractDtoRepository<T> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findById(id.trim());
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public T findByIndex(@Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize()) throw new IndexIncorrectException();
        @NotNull final IAbstractDtoRepository<T> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findByIndex(index);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize() {
        @NotNull final IAbstractDtoRepository<T> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

}