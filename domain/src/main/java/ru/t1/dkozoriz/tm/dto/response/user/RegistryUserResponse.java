package ru.t1.dkozoriz.tm.dto.response.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.response.AbstractResponse;
import ru.t1.dkozoriz.tm.dto.model.UserDto;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegistryUserResponse extends AbstractResponse {

    @Nullable
    private UserDto user;

}