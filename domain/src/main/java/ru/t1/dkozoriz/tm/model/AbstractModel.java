package ru.t1.dkozoriz.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public class AbstractModel {

    @NotNull
    @Id
    @Column(name = "row_id")
    private String id = UUID.randomUUID().toString();

}
