package ru.t1.dkozoriz.tm.exception.user;

import ru.t1.dkozoriz.tm.exception.field.AbstractFieldException;

public final class LoginEmptyException extends AbstractFieldException {

    public LoginEmptyException() {
        super("Error! Login is empty.");
    }

}