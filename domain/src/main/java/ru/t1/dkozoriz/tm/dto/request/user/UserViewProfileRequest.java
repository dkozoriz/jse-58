package ru.t1.dkozoriz.tm.dto.request.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserViewProfileRequest extends AbstractUserRequest {

    public UserViewProfileRequest(@Nullable final String token) {
        super(token);
    }

}