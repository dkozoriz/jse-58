package ru.t1.dkozoriz.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.api.service.*;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;
import ru.t1.dkozoriz.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.dkozoriz.tm.exception.system.CommandNotSupportedException;
import ru.t1.dkozoriz.tm.listener.AbstractListener;
import ru.t1.dkozoriz.tm.util.SystemUtil;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    @NotNull
    @Getter
    private ILoggerService loggerService;

    @Autowired
    @NotNull
    @Getter
    private IPropertyService propertyService;

    @Autowired
    @NotNull
    @Getter
    private ITokenService tokenService;

    private boolean processArguments(@Nullable final String[] arguments) {
        if (arguments == null || arguments.length < 1) return false;
        processArgument(arguments[0]);
        return true;
    }

    public void processCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return;
        for (@NotNull final AbstractListener listener : listeners) {
            if (command.equals(listener.getName())) {
                publisher.publishEvent(new ConsoleEvent(command));
                return;
            }
        }
        throw new CommandNotSupportedException();
    }

    private void processArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return;
        for (@NotNull final AbstractListener listener : listeners) {
            if (argument.equals(listener.getArgument())) {
                publisher.publishEvent(new ConsoleEvent(argument));
                return;
            }
        }
        throw new ArgumentNotSupportedException();
    }


    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void prepareStartup() {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        initPID();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
    }


    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @Nullable final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]\n");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]\n");
            }
        }
    }

    public void run(@Nullable final String... args) {
        if (processArguments(args)) System.exit(0);
        prepareStartup();
        processCommands();
    }

}